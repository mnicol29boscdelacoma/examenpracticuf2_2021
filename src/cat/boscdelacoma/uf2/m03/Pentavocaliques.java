package cat.boscdelacoma.uf2.m03;

import java.util.Scanner;

/**
 * Programa que demana una paraula i escriu "SI" si és pentavocàlica o bé "NO"
 * en cas contrari
 * 
 * @author marc
 */
public class Pentavocaliques {

    private static Scanner lector;

    public static void main(String[] args) {

        lector = new Scanner(System.in);
        
        System.out.println(esPentavocalica(lector.nextLine()) ? "SI" : "NO");
    }

    /**
     * Indica si la paraula indicada és pentavocàlica.
     * 
     * @param paraula La paraula que es vol comprovar
     * @return true si és una paraula pentavocàlica, false en cas contrari
     */
    static boolean esPentavocalica(String paraula) {
        int[] comptadorVocals = new int[5];

        // recorrer tots els caracters de la paraula
        for (int i = 0; i < paraula.length(); i++) {
            switch (paraula.charAt(i)) {
                case 'a':
                    comptadorVocals[0]++;
                    break;
                case 'e':
                    comptadorVocals[1]++;
                    break;
                case 'i':
                    comptadorVocals[2]++;
                    break;
                case 'o':
                    comptadorVocals[3]++;
                    break;
                case 'u':
                    comptadorVocals[4]++;
                    break;
            }
        }
        return !conteZero(comptadorVocals);
    }
    
    /**
     * Indica si un array conté algun zero en alguna posició
     * 
     * @param array L'array que es vol comprovar
     * @return true si ha trobat algun 0, false en cas contrari
     */
    static boolean conteZero(int[] array) {
        boolean trobat = false;
        int i = 0;
        
        while (i < array.length && !trobat) {
            trobat = array[i++] == 0;
        }

        return trobat;
    }

}
