package cat.boscdelacoma.uf2.m03;

import java.util.Scanner;

/**
 * Llegeix una quantitat de teclat i a continuació llegeix tants enters com la
 * quantitat indicada. Aquests enters representen les mides de diferents engrunes
 * de pa. El programa indica si hi ha alguna engruna amb una mida igual a la suma
 * de les engrunes que té a la seva dreta, o bé si no hi ha cap engruna que 
 * compleixi aquesta condició.
 * 
 * @author marc
 */
public class EntrepansFormiga {

    private static Scanner lector;
    
    public static void main(String[] args) {
        int nDades;
        
        lector = new Scanner(System.in);
        nDades = lector.nextInt();

        int posicio = comprovarEngrunes(llegirEngrunes(nDades));
        if(posicio == -1) {
            System.out.println("NO");
        } else {
            System.out.println("SI " + (posicio + 1));
        }        
    }
    
    /**
     * Llegeix de teclat una quantitat d'engrunes i les retorna posades dins un
     * array
     * 
     * @param quantitat La quantitat de valors a llegir
     * @return L'array que conté les mides de les engrunes llegides
     */
    static int[] llegirEngrunes(int quantitat) {
        int[] dades = new int[quantitat];
        
        for (int i = 0; i < quantitat; i++) {
            dades[i] = lector.nextInt();            
        }
        return dades;
    }
    
    /**
     * Comprova si hi ha alguna engruna que tingui una mida igual a la suma de 
     * les mides de les engrunes que té a la seva dreta.
     * 
     * @param engrunes L'array que conté les engrunes
     * @return La posició on hi ha l'engruna que té la mida cercada o bé -1 si 
     * no hi ha cap engruna que compleixi la condició.
     */
    static int comprovarEngrunes(int[] engrunes) {
        boolean trobat = false;
        int i = 0, suma;
        
        while (i < engrunes.length - 1 && !trobat) {
            suma = sumaPosicions(engrunes, i + 1, engrunes.length - 1);
            if(suma == engrunes[i]) {
                trobat = true;
            } else {
                i++;
            }
        }
        if(trobat) {
            return i;
        } else {
            return -1;
        }
    }
    /**
     * Suma les posicions de l'interval indicat d'un array d'engrunes.
     * 
     * @param engrunes array que conté les engrunes
     * @param ini   posició inicial on ha de començar a sumar
     * @param fi    posició final on ha d'acabr de sumar. ini <= fi
     * @return la suma de les posicions de l 'interval indicat
     */
    static int sumaPosicions(int[] engrunes, int ini, int fi) {
        int suma = 0;
        
        for (int i = ini; i <= fi; i++) {
            suma += engrunes[i];
        }
        return suma;
    }
}
